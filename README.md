# CodeBoxLaTex

[![pipeline status](https://gitlab.com/onex7zero/codeboxlatex/badges/main/pipeline.svg)](https://gitlab.com/onex7zero/codeboxlatex/-/commits/main)
[![coverage report](https://gitlab.com/onex7zero/codeboxlatex/badges/main/coverage.svg)](https://gitlab.com/onex7zero/codeboxlatex/-/commits/main)
[![Latest Release](https://gitlab.com/onex7zero/codeboxlatex/-/badges/release.svg)](https://gitlab.com/onex7zero/codeboxlatex/-/releases)

## Install
```bash
$ sudo apt update
$ sudo apt install texlive
```

## Create/Edit example.tex

## Generate PDF
```bash
$ pdflatex example.tex
```